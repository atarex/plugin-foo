<?php

Route::group(['prefix' => core_prefix('bundles/atarex/plugin-foo'), 'as' => 'atarex.plugin-foo'], function()
{

    Route::get('index', ['as' => '.index', function()
    {

        return 'Foo Bar Biz Baz ...';

    }]);

});